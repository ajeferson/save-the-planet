package br.com.savetheplanet.model;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCTextureCache;

import br.com.savetheplanet.enuns.TrashType;
import static br.com.savetheplanet.DeviceSettings.*;

public class Bin extends CCSprite{
	
	private float posX = screenWidth()/2.0f;
	private float posY = 70;
	private TrashType binType;

	public Bin(){
		super(TrashType.PAPER.getBinImage());
		this.binType = TrashType.PAPER;
		this.setPosition(this.posX, this.posY);
	}
	
	/**
	 * Change the bin type according to the order
	 * of the circle bin.
	 * */
	public void changeBinType(){
		switch (binType) {
		case PAPER:
			this.binType = TrashType.ORGANIC;
			break;
		case ORGANIC:
			this.binType = TrashType.PLASTIC;
			break;
		case PLASTIC:
			this.binType = TrashType.METAL;
			break;
		case METAL:
			this.binType = TrashType.GLASS;
			break;
		default:
			this.binType = TrashType.PAPER;
			break;
		}
	}
	
	/**
	 * Change the bin image according to
	 * the current selected bin
	 * */
	public void changeBinImage(){
		this.setTexture(CCTextureCache.
				sharedTextureCache().addImage(
						this.binType.getBinImage()));
	}
	
	public float getPosX() {
		return posX;
	}
	public void setPosX(float posX) {
		this.posX = posX;
	}
	public float getPosY() {
		return posY;
	}
	public void setPosY(float posY) {
		this.posY = posY;
	}
	
	public TrashType getBinType() {
		return binType;
	}

	public void setBinType(TrashType binType) {
		this.binType = binType;
	}
	
}
