package br.com.savetheplanet.model;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCRotateBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCTextureCache;

import br.com.savetheplanet.enuns.TrashType;
import static br.com.savetheplanet.DeviceSettings.screenHeight;

/**
 * Indicates which kind of bin is currently selected.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class CircleBin extends CCSprite{
	
	private float posX = 40;
	private float posY = screenHeight()-67f;
	private TrashType circleBinType;
	private final float ROTATE_SPEED = 0.15F;
	private final int ROTATE_ANGLE = 72;
	
	public CircleBin(){
		super(TrashType.PAPER.getCircleBinImage());
		this.circleBinType = TrashType.PAPER;
		CCDirector.sharedDirector().setScreenSize(320, 480);
		this.setPosition(this.posX, this.posY);
	}

	/**
	 * Change the circle bin type according to the order.
	 * */
	public void changeCircleBinType(){
		switch (this.circleBinType) {
		case PAPER:
			this.circleBinType = TrashType.ORGANIC;
			break;
		case ORGANIC:
			this.circleBinType = TrashType.PLASTIC;
			break;
		case PLASTIC:
			this.circleBinType = TrashType.METAL;
			break;
		case METAL:
			this.circleBinType = TrashType.GLASS;
			break;
		default:
			this.circleBinType = TrashType.PAPER;
			break;
		}
	}
	
	/**
	 * Rotates to the next circle bin.
	 * */
	public void rotateCircleBin(){
		CCRotateBy rotate = CCRotateBy.action(ROTATE_SPEED, ROTATE_ANGLE);
		CCCallFunc func = CCCallFunc.action(this, "changeCircleBinImage");
		this.runAction(CCSequence.actions(rotate, func));
	}
	
	/**
	 * Change the circle bin image according
	 * to the current selected circle bin.
	 * */
	public void changeCircleBinImage(){
		this.setRotation(0);
		this.setTexture(CCTextureCache.
				sharedTextureCache().addImage(
						this.circleBinType.getCircleBinImage()));
	}
	
	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public TrashType getCircleBinType() {
		return circleBinType;
	}

	public void setCircleBinType(TrashType circleBinType) {
		this.circleBinType = circleBinType;
	}

}
