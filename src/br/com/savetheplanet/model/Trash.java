package br.com.savetheplanet.model;

import static br.com.savetheplanet.DeviceSettings.*;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCFadeOut;
import org.cocos2d.actions.interval.CCScaleBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

import br.com.savetheplanet.delegate.TrashDelegate;
import br.com.savetheplanet.enuns.TrashOption;

public class Trash extends CCSprite{

	private TrashOption trashOption;
	private float posX, posY;
	private TrashDelegate delegate;
	private float speed;
	
	public Trash(TrashOption trashOption, float speed){
		super(trashOption.getTrashImage());
		this.trashOption = trashOption;
		this.setPosX(screenWidth()/2.0f);
		this.setPosY(screenHeight());
		this.speed = speed;
	}
	
	public void start(){
		this.schedule("update");
	}
	
	public void update(float dt){
		this.posY-=speed;
		this.setPosition(screenResolution(
				CGPoint.ccp(this.posX, this.posY)));
	}

	/**
	 * Method called when the trash reaches the bin.
	 * */
	public void trashed(){
		this.delegate.removeTrash(this);
		this.unschedule("update");
		float dt = 0.2f;
		CCScaleBy a1 = CCScaleBy.action(dt, 0.5f);
		CCFadeOut a2 = CCFadeOut.action(dt);
		CCSpawn s1 = CCSpawn.actions(a1, a2);
		CCCallFunc c1 = CCCallFunc.action(this, "removeMe");
		this.runAction(CCSequence.actions(s1, c1));
	}
	
	@Override
	public String toString() {
		return this.getTrashOption().getDescription() + " - " 
				+ this.getTrashOption().getTrashType().getDescription();
	}
	
	/**
	 * Used for Cocos2D to collect the new ununsed object
	 * and releases memory.
	 * */
	public void removeMe(){
		this.removeFromParentAndCleanup(true);
	}
	
	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public TrashDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(TrashDelegate delegate) {
		this.delegate = delegate;
	}

	public TrashOption getTrashOption() {
		return trashOption;
	}

	public void setTrashOption(TrashOption trashOption) {
		this.trashOption = trashOption;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
}
