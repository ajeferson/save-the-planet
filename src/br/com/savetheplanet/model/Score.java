package br.com.savetheplanet.model;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.opengl.CCBitmapFontAtlas;
import static br.com.savetheplanet.DeviceSettings.*;

public class Score extends CCLayer{
	
	private int score;
	private CCBitmapFontAtlas text;
	
	public Score(){
		this.score = 0;
		this.text = CCBitmapFontAtlas.
				bitmapFontAtlas(
						String.valueOf(this.score), "fonarto.fnt");
		this.text.setScale(0.8f);
		this.setPosition(screenWidth()-50, screenHeight()-50);
		this.addChild(this.text);
	}
	
	public void increaseScore(){
		this.score++;
		this.text.setString(String.valueOf(this.score));
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
