package br.com.savetheplanet.model;

import org.cocos2d.nodes.CCSprite;

import br.com.savetheplanet.delegate.CloudDelegate;
import br.com.savetheplanet.enuns.CloudOption;
import static br.com.savetheplanet.DeviceSettings.screenWidth;

/**
 * Represents a cloud Sprite.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class Cloud extends CCSprite{
	
	private float posX;
	private float posY;
	private CloudOption cloudOption;
	private boolean toRight;
	private final float CLOUD_SPEED = 0.05f;
	private final int CLOUD_OFFSET = 140;
	private CloudDelegate cloudDelegate;
	
	public Cloud(CloudOption cloudOption, float posY, boolean toRight){
		super(cloudOption.getCloudImageFile());
		this.setCloudOption(cloudOption);
		this.toRight = toRight;
		if(this.toRight){
			this.posX = -CLOUD_OFFSET;
		} else{
			this.posX = screenWidth()+CLOUD_OFFSET;
		}
		this.posY = posY;
		this.setPosition(this.posX, this.posY);
	}
	
	public Cloud(CloudOption cloudOption, float posY, float posX, boolean toRight){
		super(cloudOption.getCloudImageFile());
		this.setCloudOption(cloudOption);
		this.toRight = toRight;
		this.posX = posX;
		this.posY = posY;
		this.setPosition(this.posX, this.posY);
	}
	
	public void start(){
		this.schedule("update");
	}
	
	public void update(float dt){
		if(this.toRight){
			this.posX+=CLOUD_SPEED;
		} else{
			this.posX-=CLOUD_SPEED;
		}
		this.setPosition(this.posX, this.posY);
		if((this.toRight && this.posX-CLOUD_OFFSET>screenWidth()) || 
				(!this.toRight && this.posX+CLOUD_OFFSET<0)){
			this.cloudDelegate.removeCloud(this);
			this.unschedule("update");
			this.removeFromParentAndCleanup(true);
		}
	}
	
	public float getPosX() {
		return posX;
	}
	
	public void setPosX(float posX) {
		this.posX = posX;
	}
	
	public float getPosY() {
		return posY;
	}
	
	public void setPosY(float posY) {
		this.posY = posY;
	}

	public CloudOption getCloudOption() {
		return cloudOption;
	}

	public void setCloudOption(CloudOption cloudOption) {
		this.cloudOption = cloudOption;
	}

	public CloudDelegate getCloudDelegate() {
		return cloudDelegate;
	}

	public void setCloudDelegate(CloudDelegate cloudDelegate) {
		this.cloudDelegate = cloudDelegate;
	}

}
