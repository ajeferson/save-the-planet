package br.com.savetheplanet.enuns;

import br.com.savetheplanet.R;

public enum TrashType {

	PAPER("Paper", "bin_paper.png", "circle_bin_paper.png", R.raw.paper_effect),
	ORGANIC("Organic", "organic_bin.png", "circle_bin_organic.png", R.raw.organic_effect),
	PLASTIC("Plastic", "plastic_bin.png", "circle_bin_plastic.png", R.raw.plastic_effect),
	METAL("Metal", "metal_bin.png", "circle_bin_metal.png", R.raw.metal_effect),
	GLASS("Glass", "glass_bin.png", "circle_bin_glass.png", R.raw.glass_effect);
	
	private String description;
	private String binImage;
	private String circleBinImage;
	private int trashEffect;

	private TrashType(String description, String binImage, String circleBinImage, int trashEffect){
		this.description = description;
		this.binImage = binImage;
		this.circleBinImage = circleBinImage;
		this.trashEffect = trashEffect;
	}
	
	public String getDescription() {
		return description;
	}

	public String getBinImage() {
		return binImage;
	}
	
	public String getCircleBinImage() {
		return circleBinImage;
	}

	public int getTrashEffect() {
		return trashEffect;
	}
	
}
