package br.com.savetheplanet.enuns;

/**
 * Enumerate all the possible kinds of trash.
 * */
public enum TrashOption {

	CAN("Can", TrashType.METAL, "can.png"),
	BOTTLE("Bottle", TrashType.GLASS, "bottle.png"),
	MILK_BOX("Milk Box", TrashType.PAPER, "milk_box.png"),
	BANANA("Banana", TrashType.ORGANIC, "banana.png"),
	BAG("Bag", TrashType.PAPER, "bag_png"),
	PLASTIC_BOTTLE("Plastic Bottle", TrashType.PLASTIC, "plastic_bottle.png");
	
	private String description;
	private TrashType trashType;
	private String trashImage;
	
	private TrashOption(String description, TrashType trashType, String trashImage){
		this.description = description;
		this.trashType = trashType;
		this.trashImage = trashImage;
	}

	public String getDescription() {
		return description;
	}

	public TrashType getTrashType() {
		return trashType;
	}

	public String getTrashImage() {
		return trashImage;
	}
	
}
