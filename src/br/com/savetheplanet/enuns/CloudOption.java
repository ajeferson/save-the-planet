package br.com.savetheplanet.enuns;

/**
 * Enumerates all the possible kinds of cloud.
 * @author Alan Jeferson
 * @version 1.0
 * */
public enum CloudOption {

	CLOUD_1("Cloud 1", "cloud_1.png"),
	CLOUD_2("Cloud 2", "cloud_2.png"),
	CLOUD_3("Cloud 3", "cloud_3.png"),
	CLOUD_4("Cloud 4", "cloud_4.png"),
	CLOUD_5("Cloud 5", "cloud_5.png");
	
	
	private String description;
	private String cloudImageFile;
	
	private CloudOption(String description, String cloudImageFile){
		this.description = description;
		this.cloudImageFile = cloudImageFile;
	}

	public String getDescription() {
		return description;
	}

	public String getCloudImageFile() {
		return cloudImageFile;
	}
	
}
