package br.com.savetheplanet.menu;

import org.cocos2d.nodes.CCSprite;

/**
 * Game's background sprite.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class ScreenBackground extends CCSprite{
	
	public ScreenBackground(String image){
		super(image);
	}

}
