package br.com.savetheplanet.menu;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.types.CGPoint;


import static br.com.savetheplanet.DeviceSettings.*;

/**
 * Layer that contains all the buttons from the menu.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class MenuButtons extends CCLayer {
	
	private Button playButton;
	private Button highscoresButton;
	private Button aboutButton;
	private Button soundButton;
	private Button achievementsButton;
	
	public MenuButtons(){
		this.setIsTouchEnabled(true);
		this.playButton = new Button("Play", 0.52f);
		this.highscoresButton = new Button("Highscores", 0.52f);
		this.achievementsButton = new Button("Achievements", 0.52f);
		this.aboutButton = new Button("About", 0.52f);
		this.soundButton = new Button("Sound on", 0.45f);
		
		/* Setting buttons position */
		setButtonsPosition();
		
		/* Adding Buttons to the layer */
		this.addChild(this.playButton);
		this.addChild(this.highscoresButton);
		this.addChild(this.achievementsButton);
		this.addChild(this.aboutButton);
		this.addChild(this.soundButton);
		
		/* Setting Delegates */
		this.playButton.setDelegate(new PlayButtonDelegate());
	}
	
	private void setButtonsPosition(){
		this.playButton.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()/2.0f, 
						screenHeight()-230)));
		this.highscoresButton.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()/2.0f, screenHeight()-280f)));
		this.achievementsButton.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()/2.0f, screenHeight()-330f)));
		this.aboutButton.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()/2.0f, screenHeight()-380f)));
		this.soundButton.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()-80f, 30)));
	}

}
