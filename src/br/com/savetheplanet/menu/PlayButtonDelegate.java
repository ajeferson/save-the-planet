package br.com.savetheplanet.menu;

import org.cocos2d.nodes.CCDirector;
import org.cocos2d.transitions.CCFadeTransition;


import br.com.savetheplanet.delegate.ButtonDelegate;
import br.com.savetheplanet.scene.GameScene;

/**
 * Define what happens when the "Play Button" is pressed.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class PlayButtonDelegate implements ButtonDelegate{

	@Override
	public void buttonClicked(Button sender) {
		CCDirector.sharedDirector().replaceScene(
				CCFadeTransition.transition(
						1.0f, new GameScene().createGame()));
	}

}
