package br.com.savetheplanet.menu;

import org.cocos2d.events.CCTouchDispatcher;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCBitmapFontAtlas;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import br.com.savetheplanet.Assets;
import br.com.savetheplanet.delegate.ButtonDelegate;

import android.view.MotionEvent;

/**
 * Class que represents a button from the menu screen.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class Button extends CCLayer{
	
	private CCBitmapFontAtlas button;
	private ButtonDelegate delegate;
	
	public Button(String buttonText, float scale){
		this.setIsTouchEnabled(true);
		this.button = CCBitmapFontAtlas.bitmapFontAtlas(buttonText, Assets.FONT_MENU);
		this.button.setScale(scale);
		this.addChild(this.button);
	}

	@Override
	protected void registerWithTouchDispatcher() {
		CCTouchDispatcher.sharedDispatcher().addTargetedDelegate(this, 0, false);
	}
	
	@Override
	public boolean ccTouchesBegan(MotionEvent event) {
		CGPoint location = CGPoint.make(event.getX(), event.getY());
		location = CCDirector.sharedDirector().convertToGL(location);
		location = this.convertToNodeSpace(location);
		if(CGRect.containsPoint(this.button.getBoundingBox(), location)){
			if(this.delegate!=null){
				this.delegate.buttonClicked(this);
			}
		}
		return true;
	}
	
	public ButtonDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(ButtonDelegate delegate) {
		this.delegate = delegate;
	}

}
