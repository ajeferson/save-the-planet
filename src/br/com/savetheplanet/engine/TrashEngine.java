package br.com.savetheplanet.engine;

import java.util.Random;

import org.cocos2d.layers.CCLayer;

import br.com.savetheplanet.delegate.TrashDelegate;
import br.com.savetheplanet.enuns.TrashOption;
import br.com.savetheplanet.model.Trash;

/**
 * Engine that controls when a new trash must appear.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class TrashEngine extends CCLayer{
	
	private static Random random;
	//private static final int PROBABILITY = 40;
	private static final TrashOption[] trashOptions = TrashOption.values();
	private float speed;
	private float speedTrash;
	
	private TrashDelegate delegate;
	
	public TrashEngine(){
		this.speed = 3.5f;
		this.speedTrash = 1;
		this.schedule("trashEngine", speed);
		random = new Random();
	}
	
	public void trashEngine(float dt){
		//if(random.nextInt(PROBABILITY)==0){
			this.delegate.createTrash(
					new Trash(trashOptions
							[random.nextInt(trashOptions.length)], this.speedTrash));
			if(speed>1.5){
				this.speed-=0.1f;
				this.unschedule("trashEngine");
				this.schedule("trashEngine", speed);
			}
			if(this.speedTrash<10){
				this.speedTrash+=0.1f;
			}
		//}
	}

	public TrashDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(TrashDelegate delegate) {
		this.delegate = delegate;
	}

}
