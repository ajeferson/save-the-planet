package br.com.savetheplanet.engine;

import java.util.Random;

import org.cocos2d.layers.CCLayer;

import br.com.savetheplanet.delegate.CloudDelegate;
import br.com.savetheplanet.enuns.CloudOption;
import br.com.savetheplanet.model.Cloud;
import static br.com.savetheplanet.DeviceSettings.screenHeight;
import static br.com.savetheplanet.DeviceSettings.screenWidth;;

/**
 * Define when to create new clouds.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class CloudEngine extends CCLayer{
	
	private static Random random;
	private static CloudOption[] cloudOptions;
	private static final int PROBABILITY = 10;
	private CloudDelegate cloudDelegate;
	private final static int MAX_INITIAL_CLOUDS = 3;
	private final static int MAX_CLOUDS_HEIGHT = 220;
	private final static int MIN_CLOUDS_HEIGHT = 160;
	
	public CloudEngine(){
		this.schedule("cloudEngine", 4f);
		random = new Random();
		cloudOptions = CloudOption.values();
	}
	
	public void cloudEngine(float dt){
		if(random.nextInt(PROBABILITY)==0){
			int posY = random.nextInt(Math.round(screenHeight())-MAX_CLOUDS_HEIGHT)
					+MIN_CLOUDS_HEIGHT;
			this.cloudDelegate.createCloud(
					new Cloud(cloudOptions[random.nextInt(
							cloudOptions.length)], posY,
							random.nextBoolean()));
		}
	}

	/**
	 * Create the first initial clouds on the game.
	 * */
	public void createInitialClouds(){
		for(int i=0; i<MAX_INITIAL_CLOUDS; i++){
			if(random.nextBoolean()){
				int posY = random.nextInt(Math.round(screenHeight())-MAX_CLOUDS_HEIGHT)
						+MIN_CLOUDS_HEIGHT;
				int posX = random.nextInt(Math.round(screenWidth()));
				this.cloudDelegate.createCloud(new Cloud(cloudOptions[random.nextInt(
						cloudOptions.length)], posY, posX,
						random.nextBoolean()));
			}
		}
	}
	
	public CloudDelegate getCloudDelegate() {
		return cloudDelegate;
	}

	public void setCloudDelegate(CloudDelegate cloudDelegate) {
		this.cloudDelegate = cloudDelegate;
	}

}
