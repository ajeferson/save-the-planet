package br.com.savetheplanet.delegate;

import br.com.savetheplanet.menu.Button;

/**
 * Interface that defines a method for when
 * a menu button is pressed.
 * @author Alan Jeferson
 * @version 1.0
 * */
public interface ButtonDelegate {

	public void buttonClicked(Button sender);
	
}
