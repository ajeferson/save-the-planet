package br.com.savetheplanet.delegate;

import br.com.savetheplanet.model.Trash;

/**
 * Interface that defines a method to be
 * called when the next gameloop happens.
 * @author Alan Jeferson
 * @version 1.0
 * */
public interface TrashDelegate {
	
	public void createTrash(Trash trash);
	public void removeTrash(Trash trash);

}
