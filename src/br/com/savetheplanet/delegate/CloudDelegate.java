package br.com.savetheplanet.delegate;

import br.com.savetheplanet.model.Cloud;

/**
 * Delegate for creating a cloud.
 * @author Alan Jeferson.
 * @version 1.0
 * */
public interface CloudDelegate {

	public void createCloud(Cloud cloud);
	public void removeCloud(Cloud cloud);
	
}
