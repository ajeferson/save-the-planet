package br.com.savetheplanet.scene;

import static br.com.savetheplanet.DeviceSettings.screenHeight;
import static br.com.savetheplanet.DeviceSettings.screenResolution;
import static br.com.savetheplanet.DeviceSettings.screenWidth;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCBitmapFontAtlas;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;

import br.com.savetheplanet.Assets;
import br.com.savetheplanet.R;
import br.com.savetheplanet.delegate.ButtonDelegate;
import br.com.savetheplanet.menu.Button;
import br.com.savetheplanet.menu.ScreenBackground;

public class GameOverScene extends CCLayer implements ButtonDelegate{
	
	private CCBitmapFontAtlas gameover;
	private CCBitmapFontAtlas scoreText;
	private int score;
	private Button playAgainButton;
	private Button menuButton;
	private ScreenBackground background;
	private CCLayer layer;
	
	public GameOverScene(int score){
		this.score = score;
		this.background = new ScreenBackground(Assets.BACKGROUND);
		this.background.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()/2.0f, 
						screenHeight()/2.0f)));
		this.addChild(this.background);
		
		this.layer = CCLayer.node();
		this.addChild(this.layer);
		
		this.addGameObjects();
		
		SoundEngine.sharedEngine().playSound(CCDirector.sharedDirector().getActivity(), 
				R.raw.game_over, false);
		
	}
	
	private void addGameObjects(){
		/* Game Over text */
		this.gameover = CCBitmapFontAtlas.bitmapFontAtlas("Game Over :(", Assets.FONT_GAME_OVER);
		this.gameover.setScale(0.7f);
		this.gameover.setPosition(screenWidth()/2f, screenHeight()-70);
		this.layer.addChild(this.gameover);
		
		/* Score */
		this.scoreText = CCBitmapFontAtlas.bitmapFontAtlas(
				String.valueOf(this.score), Assets.FONT_GAME_OVER);
		this.scoreText.setPosition(screenWidth()/2f, screenHeight()-175);
		this.layer.addChild(this.scoreText);
		
		/* Play Again Button */
		this.playAgainButton = new Button("Play again", 0.55f);
		this.playAgainButton.setPosition(screenWidth()/2f, screenHeight()-290);
		this.playAgainButton.setDelegate(this);
		this.layer.addChild(this.playAgainButton);
		
		/* Exit Button */
		this.menuButton = new Button("Menu", 0.55f);
		this.menuButton.setPosition(screenWidth()/2f, screenHeight()-340);
		this.menuButton.setDelegate(this);
		this.layer.addChild(this.menuButton);
	}
	
	@Override
	public void buttonClicked(Button sender) {
		if(sender.equals(this.playAgainButton)){
			CCDirector.sharedDirector().replaceScene(
					CCFadeTransition.transition(
							1.0f, new GameScene().createGame()));
		} else if(sender.equals(this.menuButton)){
			CCDirector.sharedDirector().replaceScene(
					CCFadeTransition.transition(
							1.0f, new MenuScene().scene()));
		}
	}
	
	public CCScene scene(){
		CCScene scene = CCScene.node();
		scene.addChild(this);
		return scene;
	}

}
