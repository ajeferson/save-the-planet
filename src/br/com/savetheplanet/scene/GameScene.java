package br.com.savetheplanet.scene;

import static br.com.savetheplanet.DeviceSettings.screenHeight;
import static br.com.savetheplanet.DeviceSettings.screenResolution;
import static br.com.savetheplanet.DeviceSettings.screenWidth;

import java.util.ArrayList;
import java.util.List;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import br.com.savetheplanet.Assets;
import br.com.savetheplanet.MainActivity;
import br.com.savetheplanet.R;
import br.com.savetheplanet.delegate.CloudDelegate;
import br.com.savetheplanet.delegate.TrashDelegate;
import br.com.savetheplanet.engine.CloudEngine;
import br.com.savetheplanet.engine.TrashEngine;
import br.com.savetheplanet.enuns.TrashType;
import br.com.savetheplanet.menu.ScreenBackground;
import br.com.savetheplanet.model.Cloud;
import br.com.savetheplanet.model.Score;
import br.com.savetheplanet.model.Trash;

/**
 * Game scene, when the game actually happens.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class GameScene extends CCLayer implements TrashDelegate, CloudDelegate{

	private final int BIN_BOTTOM = 10;
	
	private ScreenBackground background;
	private TrashEngine trashEngine;
	private CCLayer trashLayer;
	private List<Trash> trashList;
	private BinLayer binLayer;
	private CCLayer scoreLayer;
	private Score score;
	private List<Cloud> cloudList;
	private CloudEngine cloudEngine;
	private CCLayer cloudLayer;

	public GameScene(){
		/* Background */
		this.background = new ScreenBackground(Assets.BACKGROUND);
		this.background.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()/2.0f, 
						screenHeight()/2.0f)));
		this.addChild(this.background);
		
		/* Cloud */
		this.cloudLayer = CCLayer.node();
		this.addChild(this.cloudLayer);

		/* Trash */
		this.trashLayer = CCLayer.node();
		this.addChild(this.trashLayer);

		/* Bin */
		this.binLayer = new BinLayer();
		this.addChild(this.binLayer);
		
		/* Score */
		this.scoreLayer = CCLayer.node();
		this.addChild(this.scoreLayer);
		
		/* Adding game objects */
		this.addGameObjects();
		
		/* Caching effects */
		this.preloadEffects();
		
		/* Playing background sound */
		this.playBackgroundSound();
		
		((MainActivity) CCDirector.sharedDirector().getActivity()).setScene(this);
	}

	public CCScene createGame(){
		CCScene scene = CCScene.node();
		scene.addChild(this);
		return scene;
	}

	@Override
	public void createTrash(Trash trash) {
		trash.setDelegate(this);
		this.trashLayer.addChild(trash);
		this.trashList.add(trash);
		trash.start();
	}
	
	@Override
	public void removeTrash(Trash trash) {	
		this.trashList.remove(trash);
	}

	@Override
	public void onEnter() {
		super.onEnter();
		this.schedule("checkHits");
		this.startEngines();
	}

	private void startEngines(){
		this.addChild(this.trashEngine);
		this.addChild(this.cloudEngine);
		this.trashEngine.setDelegate(this);
		this.cloudEngine.setCloudDelegate(this);
		this.cloudEngine.createInitialClouds();
	}

	private void addGameObjects(){
		/* Trash */
		this.trashList = new ArrayList<Trash>();
		this.trashEngine = new TrashEngine();

		/* Bin */
		this.binLayer.createObjects();
		
		/* Score */
		this.score = new Score();
		this.scoreLayer.addChild(this.score);
		
		/* Clouds */
		this.cloudList = new ArrayList<Cloud>();
		this.cloudEngine = new CloudEngine();
	}	

	/**
	 * Get the rectangle of a trash object.
	 * */
	public CGRect getTrashBorders(CCSprite object){
		CGRect rect = object.getBoundingBox();
		CGPoint GLpoint = rect.origin;
		CGRect GLrect = CGRect.make(GLpoint.x, GLpoint.y, rect.size.width,
				rect.size.height);
		return GLrect;
	}
	
	/**
	 * Get the rectangle of the bin's bottom.
	 * */
	public CGRect getBinBorders(){
		CGRect rect = this.binLayer.getBin().getBoundingBox();
		CGPoint GLpoint = rect.origin;
		CGRect GLrect = CGRect.make(GLpoint.x, GLpoint.y-BIN_BOTTOM, rect.size.width, 10);
		return GLrect;
	}

	/**
	 * Check if some trash colides with the bin
	 * */
	private boolean checkColisions(){
		CGRect binRect = this.getBinBorders();
		CGRect trashRect;
		for(Trash trash : this.trashList){
			trashRect = this.getTrashBorders(trash);
			if(CGRect.intersects(binRect, trashRect)){
				trash.trashed();
				if(gameOver(trash)){
					this.openGameOverScene();
				} else{
					this.score.increaseScore();
					this.playBeatSong(trash);
				}
				return true;
			}
		}
		return false;
	}
	
	private void openGameOverScene(){
		//SoundEngine.sharedEngine().pauseSound();
		SoundEngine.sharedEngine().realesAllSounds();
		CCDirector.sharedDirector().replaceScene(
				new GameOverScene(this.score.getScore()).scene());
	}
	
	/**
	 * Check wether or not the player lost the game.
	 * */
	public boolean gameOver(Trash trash){
		return !this.binLayer.getBin().getBinType().
				equals(trash.getTrashOption().getTrashType());
	}
	
	/**
	 * Play the trash effect according to the trash type.
	 * */
	private void playBeatSong(Trash trash){
		SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), 
					trash.getTrashOption().getTrashType().getTrashEffect());
	}
	
	/**
	 * Plays the background sound.
	 * */
	public void playBackgroundSound(){
		SoundEngine.sharedEngine().playSound(
				CCDirector.sharedDirector().getActivity(), 
					R.raw.menu_background, true);
	}
	
	/**
	 * Make the cache of effects, for they not to take
	 * too long to play.
	 * */
	private void preloadEffects(){
		SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), 
				TrashType.GLASS.getTrashEffect());
		SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), 
				TrashType.METAL.getTrashEffect());
		SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), 
				TrashType.ORGANIC.getTrashEffect());
		SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), 
				TrashType.PAPER.getTrashEffect());
		SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), 
				TrashType.PLASTIC.getTrashEffect());
		SoundEngine.sharedEngine().preloadEffect(CCDirector.sharedDirector().getActivity(), 
				R.raw.bin_click);
	}
	
	public void checkHits(float dt){
		this.checkColisions();
	}

	/* Cloud Stuff */
	@Override
	public void createCloud(Cloud cloud) {
		this.cloudList.add(cloud);
		this.cloudLayer.addChild(cloud);
		cloud.setCloudDelegate(this);
		cloud.start();
	}

	@Override
	public void removeCloud(Cloud cloud) {
		this.cloudList.remove(cloud);
	}

}
