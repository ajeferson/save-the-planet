package br.com.savetheplanet.scene;

import static br.com.savetheplanet.DeviceSettings.screenHeight;

import org.cocos2d.events.CCTouchDispatcher;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;
import br.com.savetheplanet.Assets;
import br.com.savetheplanet.R;
import br.com.savetheplanet.model.Bin;
import br.com.savetheplanet.model.CircleBin;

/**
 * Layer responsible for placing the bin, the circle bin
 * and the arrow. It is also responsible for responding to click events
 * @author Alan Jeferson
 * @version 1.0
 * */
public class BinLayer extends CCLayer{

	private Bin bin;
	private CircleBin circleBin;
	private CCSprite arrow;

	public BinLayer(){
		this.setIsTouchEnabled(true);
	}
	
	/**
	 * Create the objects and place them on the screen
	 * */
	public void createObjects(){
		/* Bin */
		this.bin = new Bin();
		this.addChild(this.bin);
		
		/* Circle Bin */
		this.circleBin = new CircleBin();
		this.addChild(this.circleBin);
		
		/* Arrow */
		this.arrow = new CCSprite(Assets.ARROW);
		this.arrow.setPosition(40, screenHeight()-20f);
		this.addChild(this.arrow);
	}
	
	@Override
	protected void registerWithTouchDispatcher() {
		CCTouchDispatcher.sharedDispatcher().addTargetedDelegate(this, 0, false);
	}
	
	@Override
	public boolean ccTouchesBegan(MotionEvent event) {
		CGPoint location = CGPoint.make(event.getX(), event.getY());
		location = CCDirector.sharedDirector().convertToGL(location);
		location = this.convertToNodeSpace(location);
		if(CGRect.containsPoint(this.bin.getBoundingBox(), location)){
			this.changeBinThings();
		}
		return true;
	}
	
	/**
	 * Changes the bin color and rotates the circle bin.
	 * */
	private void changeBinThings(){
		SoundEngine.sharedEngine().playEffect(CCDirector.sharedDirector().getActivity(), 
				R.raw.bin_click);
		this.bin.changeBinType();
		this.bin.changeBinImage();
		this.circleBin.changeCircleBinType();
		this.circleBin.rotateCircleBin();
	}
	
	public Bin getBin() {
		return bin;
	}

	public void setBin(Bin bin) {
		this.bin = bin;
	}

	public CircleBin getCircleBin() {
		return circleBin;
	}

	public void setCircleBin(CircleBin circleBin) {
		this.circleBin = circleBin;
	}
	
}
