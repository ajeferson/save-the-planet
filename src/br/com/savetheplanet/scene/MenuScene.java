package br.com.savetheplanet.scene;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.opengl.CCBitmapFontAtlas;
import org.cocos2d.types.CGPoint;

import br.com.savetheplanet.Assets;
import br.com.savetheplanet.menu.MenuButtons;
import br.com.savetheplanet.menu.ScreenBackground;
import static br.com.savetheplanet.DeviceSettings.*;

/**
 * Menu Screen.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class MenuScene extends CCLayer{
	
	private ScreenBackground background;
	private CCBitmapFontAtlas logoText;
	
	public MenuScene(){
		this.background = new ScreenBackground(Assets.BACKGROUND);
		this.background.setPosition(screenResolution(
				CGPoint.ccp(
						screenWidth()/2.0f, 
						screenHeight()/2.0f)));
		this.addChild(background);
		
		/* Logo picture */
		CCSprite logo = new CCSprite(Assets.LOGO);
		logo.setScale(0.1f);
		logo.setPosition(screenResolution(CGPoint.ccp(screenWidth()/2.0f, screenHeight()-100f)));
		this.addChild(logo);
		
		/* Logo text */
		this.logoText = CCBitmapFontAtlas.bitmapFontAtlas("Save the planet!", Assets.FONT_LOGO);
		this.logoText.setScale(0.55f);
		this.logoText.setPosition(screenResolution(
				CGPoint.ccp(screenWidth()/2.0f, 
						screenHeight()-180f)));
		this.addChild(this.logoText);
		
		/* Adding menu */
		MenuButtons menu = new MenuButtons();
		this.addChild(menu);
	}
	
	public CCScene scene(){
		CCScene scene = CCScene.node(); //Creating a screen
		scene.addChild(this);
		return scene;
	}

}
