package br.com.savetheplanet;

import org.cocos2d.nodes.CCDirector;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGSize;

/**
 * Class that has information about
 * screen dimensions of the device.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class DeviceSettings {

	public static CGPoint screenResolution(CGPoint cgPoint){
		return cgPoint;
	}
	
	public static float screenWidth(){
		return winSize().width;
	}
	
	public static float screenHeight(){
		return winSize().height;
	}
	
	public static CGSize winSize(){
		return CCDirector.sharedDirector().winSize();
	}
	
}
