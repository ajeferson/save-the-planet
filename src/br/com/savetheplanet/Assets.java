package br.com.savetheplanet;

/**
 * Contains all the assets names of the projects.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class Assets {

	public static final String BACKGROUND = "background.png";
	public static final String LOGO = "logo_10.png";
	public static final String FONT_LOGO = "logo_font_2.fnt";
	public static final String PLAY_BUTTON = "button_play.png";
	public static final String HIGHSCORES_BUTTON = "highscores_button.png";
	public static final String ABOUT_BUTTON = "about_button.png";
	public static final String SOUND_OFF_BUTTON = "sound_off_button.png";
	public static final String SOUND_ON_BUTTON = "sound_on_button.png";	
	public static final String ARROW = "arrow.png";
	public static final String GAME_OVER = "game-over_2.png";
	public static final String FONT_GAME_OVER = "font_gameover_4.fnt";
	public static final String FONT_MENU = "font_menu.fnt";
	public static final String FONT_FINAL_SCORE = "font_final_score_6.fnt";
	
}
